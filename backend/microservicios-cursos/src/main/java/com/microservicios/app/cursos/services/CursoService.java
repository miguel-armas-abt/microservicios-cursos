package com.microservicios.app.cursos.services;

import com.microservicios.app.cursos.models.entity.Curso;
import com.microservicios.commons.alumnos.models.entity.Alumno;
import com.microservicios.commons.services.CommonService;

public interface CursoService extends CommonService<Curso>{

	// OBTENER CURSO AL QUE PERTENECE UN ALUMNO POR SU ID
	public Curso findCursoByAlumnoId(Long id);
	
	// ELIMINAR CURSO_ALUMNO POR ID
	public void eliminarCursoAlumnoPorId(Long id);
	
	// OBTENER ALUMNOS POR CURSO (cliente alumno feign)
	public Iterable<Alumno> obtenerAlumnosPorCurso(Iterable<Long> ids);
	
	// (cliente respuesta feign)
	public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId);
}
