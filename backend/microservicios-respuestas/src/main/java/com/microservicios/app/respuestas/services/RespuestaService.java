package com.microservicios.app.respuestas.services;

import com.microservicios.app.respuestas.models.entity.Respuesta;

public interface RespuestaService {

	// CREAR
	public Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas);
	
	// OBTENER RESPUESTAS DEL EXAMEN POR ALUMNO
	public Iterable<Respuesta> findRespuestaByAlumnoByExamen(Long alumnoId, Long examenId);
	
	// OBTENER EXAMENES_IDS POR ALUMNO
	public Iterable<Long> findExamenesIdsConRespuestasByAlumno(Long alumnoId);
	
	// OBTENER POR ALUMNO ID
	public Iterable<Respuesta> findByAlumnoId(Long alumnoId);
}