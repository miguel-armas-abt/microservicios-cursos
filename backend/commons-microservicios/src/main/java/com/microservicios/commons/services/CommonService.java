package com.microservicios.commons.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

// E: Entity
public interface CommonService<E> {

	// SALVAR
	public E save(E entity);
	
	// OBTENER TODOS
	public Iterable<E> findAll();
	
	// OBTENER TODOS PAGINABLE
	public Page<E> findAll(Pageable pageable);
	
	// OBTENER POR ID
	public Optional<E> findById(Long id);
	
	// ELIMINAR POR ID
	public void deleteById(Long id);
}