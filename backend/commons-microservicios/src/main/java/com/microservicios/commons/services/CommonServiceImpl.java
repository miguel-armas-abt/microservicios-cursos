package com.microservicios.commons.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

// E: Entity
// R: Repository
public class CommonServiceImpl<E, R extends PagingAndSortingRepository<E, Long>> 
	implements CommonService<E>{

	@Autowired
	protected R repository;
	
	// SALVAR
	@Override
	@Transactional
	public E save(E entity) {
		return repository.save(entity);
	}
	
	// OBTENER TODOS
	@Override
	@Transactional(readOnly = true)
	public Iterable<E> findAll() {
		return repository.findAll();
	}

	// OBTENER TODOS PAGINABLE
	// Pageable: recupera #objetos por página y el número de página pasados en el URI
	@Override
	@Transactional
	public Page<E> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	// OBTENER POR ID
	@Override
	@Transactional(readOnly = true)
	public Optional<E> findById(Long id) {
		return repository.findById(id);
	}

	// ELIMINAR POR ID
	@Override
	@Transactional
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
}
