package com.microservicios.commons.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
// import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.microservicios.commons.services.CommonService;

// E: Entity
// S: Service
// @CrossOrigin({"http://localhost:4200"})	// angular
public class CommonController<E, S extends CommonService<E>> {

	@Autowired
	protected S service;
	
	// CREAR
	// @Valid: valida los atributos del cuerpo del JSON enviado (va antes del @RequestBody)
	// BindingResult: resultado de la validación (va después del Entity)
	@PostMapping
	public ResponseEntity<?> crear(@Valid @RequestBody E entity, BindingResult result){
		if(result.hasErrors()) {
			return this.validar(result);
		}
		E entityDb = service.save(entity);
		return ResponseEntity.status(HttpStatus.CREATED).body(entityDb); // 201
	}
	
	// ELIMINAR POR ID
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id){
		service.deleteById(id);
		return ResponseEntity.noContent().build();	// 204
	}
	
	// OBTENER TODOS
	@GetMapping
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok().body(service.findAll());	// 200
	}
	
	// OBTENER TODOS PAGINABLE
	@GetMapping("/pagina")
	public ResponseEntity<?> listar(Pageable pageable){
		return ResponseEntity.ok().body(service.findAll(pageable));	// 200
	}
	
	// OBTENER POR ID
	@GetMapping("/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id){
		Optional<E> o = service.findById(id);
		if(o.isEmpty()) {
			return ResponseEntity.notFound().build();	// 400
		}
		return ResponseEntity.ok(o.get());	// 200
	}
	
	// VALIDACIONES
	protected ResponseEntity<?> validar(BindingResult result){
		Map<String, Object> errores = new HashMap<>();
		
		//obtener errores de campo
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(), "El campo "+err.getField()+" "+err.getDefaultMessage());
		});
		return ResponseEntity.badRequest().body(errores);
	}
}
