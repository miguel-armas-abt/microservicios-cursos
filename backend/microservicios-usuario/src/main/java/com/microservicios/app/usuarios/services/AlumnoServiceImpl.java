package com.microservicios.app.usuarios.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.microservicios.app.usuarios.client.CursoFeignClient;
import com.microservicios.app.usuarios.models.repository.AlumnoRepository;
import com.microservicios.commons.alumnos.models.entity.Alumno;
import com.microservicios.commons.services.CommonServiceImpl;


@Service
public class AlumnoServiceImpl extends CommonServiceImpl<Alumno, AlumnoRepository> 
	implements AlumnoService{

	@Autowired
	private CursoFeignClient clientCurso;
	
	// OBTENER POR NOMBRE O APELLIDO
	@Override
	@Transactional(readOnly = true)
	public List<Alumno> findByNombreOrApellido(String term) {
		return repository.findByNombreOrApellido(term);
	}

	// OBTENER POR IDS
	@Override
	@Transactional(readOnly = true)
	public Iterable<Alumno> findAllById(Iterable<Long> ids) {
		return repository.findAllById(ids);
	}

	// ELIMINAR POR ID
	@Override
	@Transactional	// asegura la completitud de la transacción
	public void deleteById(Long id) {
		super.deleteById(id);	// eliminar de Postgress
		this.eliminarCursoAlumnoPorId(id);	// eliminar de MySQL
	}

	// ELIMINAR CURSO_ALUMNO POR ID (cliente curso feign)
	@Override
	public void eliminarCursoAlumnoPorId(Long id) {
		clientCurso.eliminarCursoAlumnoPorId(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Alumno> findAll() {
		return repository.findAllByOrderByIdAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Alumno> findAll(Pageable pageable) {
		return repository.findAllByOrderByIdAsc(pageable);
	}
	
	
}
