package com.microservicios.app.usuarios.services;

import java.util.List;

import com.microservicios.commons.alumnos.models.entity.Alumno;
import com.microservicios.commons.services.CommonService;

public interface AlumnoService extends CommonService<Alumno>{

	// OBTENER POR NOMBRE O APELLIDO
	public List<Alumno> findByNombreOrApellido(String term);
	
	// OBTENER POR ID
	public Iterable<Alumno> findAllById(Iterable<Long> ids);
	
	// ELIMINAR CURSO_ALUMNO POR ID
	public void eliminarCursoAlumnoPorId(Long id);
	
}