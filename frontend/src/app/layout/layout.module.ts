import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [NavbarComponent],
  exports: [NavbarComponent], // para que sea utilizado por otros componentes
  imports: [
    CommonModule,
    AppRoutingModule
  ]
})

// este module debe registrarse en app.module.ts
export class LayoutModule { }
