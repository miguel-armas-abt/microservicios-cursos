import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule} from '@angular/common/http'; // apirest
import { FormsModule, ReactiveFormsModule} from '@angular/forms';  // forms
import { MatPaginatorModule } from '@angular/material/paginator'; // paginable
import {MatTableModule} from '@angular/material/table'; // table
import {MatInputModule} from '@angular/material/input'; // input
import {MatCheckboxModule} from '@angular/material/checkbox'; // checkbox
import {MatButtonModule} from '@angular/material/button'; // button
import {MatCardModule} from '@angular/material/card'; // card
import {MatTabsModule} from '@angular/material/tabs'; // tabs
import {MatAutocompleteModule} from '@angular/material/autocomplete'; // autocomplete
import {MatDialogModule} from '@angular/material/dialog'; //modal
import {MatExpansionModule} from '@angular/material/expansion';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlumnosComponent } from './components/alumnos/alumnos.component';
import { CursosComponent } from './components/cursos/cursos.component';
import { ExamenesComponent } from './components/examenes/examenes.component';
import { LayoutModule } from './layout/layout.module';
import { AlumnosFormComponent } from './components/alumnos/alumnos-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CursoFormComponent } from './components/cursos/curso-form.component';
import { ExamenFormComponent } from './components/examenes/examen-form.component';
import { AsignarAlumnosComponent } from './components/cursos/asignar-alumnos.component';
import { AsignarExamenesComponent } from './components/cursos/asignar-examenes.component';
import { ResponderExamenComponent } from './components/alumnos/responder-examen.component';
import { ResponderExamenModalComponent } from './components/alumnos/responder-examen-modal.component';
import { VerExamenModalComponent } from './components/alumnos/ver-examen-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    AlumnosComponent,
    CursosComponent,
    ExamenesComponent,
    AlumnosFormComponent,
    CursoFormComponent,
    ExamenFormComponent,
    AsignarAlumnosComponent,
    AsignarExamenesComponent,
    ResponderExamenComponent,
    ResponderExamenModalComponent,
    VerExamenModalComponent
  ],
  entryComponents: [
    ResponderExamenModalComponent, // el modal es un componente de entrada
    VerExamenModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,  // apirest
    FormsModule,  // forms
    ReactiveFormsModule,  // reactive forms
    BrowserAnimationsModule,
    MatPaginatorModule,  // paginable
    MatTableModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatDialogModule, // modal
    MatExpansionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
