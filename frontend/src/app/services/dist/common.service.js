"use strict";
exports.__esModule = true;
exports.CommonService = void 0;
var http_1 = require("@angular/common/http");
var CommonService = /** @class */ (function () {
    function CommonService(http) {
        this.http = http;
        this.cabeceras = new http_1.HttpHeaders({ 'Content-Type': 'application/json' });
    }
    CommonService.prototype.listar = function () {
        return this.http.get(this.baseEndPoint);
    };
    // listar paginable
    CommonService.prototype.listarPaginas = function (page, size) {
        var params = new http_1.HttpParams()
            .set('page', page)
            .set('size', size);
        return this.http.get(this.baseEndPoint + "/pagina", { params: params });
    };
    CommonService.prototype.ver = function (id) {
        return this.http.get(this.baseEndPoint + "/" + id);
    };
    CommonService.prototype.crear = function (e) {
        return this.http.post(this.baseEndPoint, e, { headers: this.cabeceras });
    };
    CommonService.prototype.editar = function (e) {
        return this.http.put(this.baseEndPoint + "/" + e.id, e, { headers: this.cabeceras });
    };
    CommonService.prototype.eliminar = function (id) {
        return this.http["delete"](this.baseEndPoint + "/" + id);
    };
    return CommonService;
}());
exports.CommonService = CommonService;
