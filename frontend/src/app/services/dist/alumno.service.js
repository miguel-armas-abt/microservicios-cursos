"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AlumnoService = void 0;
var core_1 = require("@angular/core");
var common_service_1 = require("./common.service");
var app_1 = require("../config/app");
var AlumnoService = /** @class */ (function (_super) {
    __extends(AlumnoService, _super);
    function AlumnoService(http) {
        var _this = _super.call(this, http) || this;
        _this.baseEndPoint = app_1.BASE_ENDPOINT + '/alumnos';
        return _this;
    }
    // file mismo nombre que en backend
    AlumnoService.prototype.crearConFoto = function (alumno, archivo) {
        var formData = new FormData();
        formData.append('archivo', archivo);
        formData.append('nombre', alumno.nombre);
        formData.append('apellido', alumno.apellido);
        formData.append('email', alumno.email);
        return this.http.post(this.baseEndPoint + '/crear-con-foto', formData);
    };
    AlumnoService.prototype.editarConFoto = function (alumno, archivo) {
        var formData = new FormData();
        formData.append('archivo', archivo);
        formData.append('nombre', alumno.nombre);
        formData.append('apellido', alumno.apellido);
        formData.append('email', alumno.email);
        return this.http.put(this.baseEndPoint + "/editar-con-foto/" + alumno.id, formData);
    };
    AlumnoService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], AlumnoService);
    return AlumnoService;
}(common_service_1.CommonService));
exports.AlumnoService = AlumnoService;
