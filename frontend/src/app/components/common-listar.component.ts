import { OnInit, ViewChild } from '@angular/core';

import Swal from 'sweetalert2';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Generic } from '../models/generic';
import { CommonService } from '../services/common.service';

// E: entity
// S: service
export abstract class CommonListarComponent<E extends Generic, S extends CommonService<E>>
  implements OnInit {

  titulo: string;
  lista: E[];

  protected nombreModel: string;

  totalRegistros = 0;
  paginaActual = 0;
  totalPorPagina = 4;
  pageSizeOptions: number[] = [2, 4, 6, 8, 10];

  constructor(protected service: S) { }

  @ViewChild(MatPaginator) paginator: MatPaginator; // paginator


  ngOnInit(): void {
    this.calcularRangos();
  }

  public paginar(event: PageEvent): void {
    this.paginaActual = event.pageIndex;
    this.totalPorPagina = event.pageSize;

    this.calcularRangos();
  }

  private calcularRangos() {

    this.service.listarPaginas(this.paginaActual.toString(), this.totalPorPagina.toString())
      .subscribe(paginable => {
        this.lista = paginable.content as E[]; // convierto el atributo content del json a un arreglo de tipo E
        this.totalRegistros = paginable.totalElements as number;
        this.paginator._intl.itemsPerPageLabel = 'Registros por página' // internacionalización
      });
  }

  public eliminar(e: E): void {
    Swal.fire({
      title: '¡Cuidado!',
      text: `¿Seguro que quiere eliminar a ${e.nombre}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.eliminar(e.id).subscribe(() => {
          this.calcularRangos();
          Swal.fire('Eliminado: ', `El ${this.nombreModel} ${e.nombre} fue eliminado`, 'success');
        });
      }
    });
  }
}