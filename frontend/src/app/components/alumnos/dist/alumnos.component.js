"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AlumnosComponent = void 0;
var core_1 = require("@angular/core");
var alumno_1 = require("../../models/alumno");
var common_listar_component_1 = require("../common-listar.component");
var app_1 = require("../../config/app");
var AlumnosComponent = /** @class */ (function (_super) {
    __extends(AlumnosComponent, _super);
    function AlumnosComponent(service) {
        var _this = _super.call(this, service) || this;
        _this.baseEndPoint = app_1.BASE_ENDPOINT + '/alumnos';
        _this.titulo = 'Lista de alumnos';
        _this.nombreModel = alumno_1.Alumno.name;
        return _this;
    }
    AlumnosComponent = __decorate([
        core_1.Component({
            selector: 'app-alumnos',
            templateUrl: './alumnos.component.html',
            styleUrls: ['./alumnos.component.css']
        })
    ], AlumnosComponent);
    return AlumnosComponent;
}(common_listar_component_1.CommonListarComponent));
exports.AlumnosComponent = AlumnosComponent;
