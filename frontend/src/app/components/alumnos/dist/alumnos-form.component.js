"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AlumnosFormComponent = void 0;
var core_1 = require("@angular/core");
var alumno_1 = require("../../models/alumno");
var sweetalert2_1 = require("sweetalert2");
var common_form_component_1 = require("../common-form.component");
var AlumnosFormComponent = /** @class */ (function (_super) {
    __extends(AlumnosFormComponent, _super);
    function AlumnosFormComponent(service, // servicio   
    router, // enrutador
    route // obtiene parametros de la ruta
    ) {
        var _this = _super.call(this, service, router, route) || this;
        _this.titulo = 'Crear alumno';
        _this.model = new alumno_1.Alumno();
        _this.redirect = '/alumnos';
        _this.nombreModel = alumno_1.Alumno.name;
        return _this;
    }
    AlumnosFormComponent.prototype.seleccionarFoto = function (event) {
        this.fotoSeleccionada = event.target.files[0];
        console.info(this.fotoSeleccionada);
        if (this.fotoSeleccionada.type.indexOf('image') < 0) {
            this.fotoSeleccionada = null;
            sweetalert2_1["default"].fire('Error al seleccionar la foto', 'El archivo debe ser de tipo imagen', 'error');
        }
    };
    AlumnosFormComponent.prototype.crear = function () {
        var _this = this;
        if (!this.seleccionarFoto) {
            _super.prototype.crear.call(this);
        }
        else {
            this.service.crearConFoto(this.model, this.fotoSeleccionada)
                .subscribe(function (alumno) {
                console.log(alumno);
                sweetalert2_1["default"].fire('Nuevo: ', _this.nombreModel + " " + alumno.nombre + " creado con \u00E9xito", 'success');
                _this.router.navigate([_this.redirect]);
            }, function (err) {
                if (err.status === 400) {
                    _this.error = err.error;
                    console.log(_this.error);
                }
            });
        }
    };
    AlumnosFormComponent.prototype.editar = function () {
        var _this = this;
        if (!this.seleccionarFoto) {
            _super.prototype.editar.call(this);
        }
        else {
            this.service.editarConFoto(this.model, this.fotoSeleccionada)
                .subscribe(function (alumno) {
                console.log(alumno);
                sweetalert2_1["default"].fire('Modificado: ', _this.nombreModel + " " + alumno.nombre + " modificado con \u00E9xito", 'success');
                _this.router.navigate([_this.redirect]);
            }, function (err) {
                if (err.status === 400) {
                    _this.error = err.error;
                    console.log(_this.error);
                }
            });
        }
    };
    AlumnosFormComponent = __decorate([
        core_1.Component({
            selector: 'app-alumnos-form',
            templateUrl: './alumnos-form.component.html',
            styleUrls: ['./alumnos-form.component.css']
        })
    ], AlumnosFormComponent);
    return AlumnosFormComponent;
}(common_form_component_1.CommonFormComponent));
exports.AlumnosFormComponent = AlumnosFormComponent;
