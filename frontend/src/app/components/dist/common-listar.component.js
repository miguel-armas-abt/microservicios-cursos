"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CommonListarComponent = void 0;
var core_1 = require("@angular/core");
var sweetalert2_1 = require("sweetalert2");
var paginator_1 = require("@angular/material/paginator");
// E: entity
// S: service
var CommonListarComponent = /** @class */ (function () {
    function CommonListarComponent(service) {
        this.service = service;
        this.totalRegistros = 0;
        this.paginaActual = 0;
        this.totalPorPagina = 4;
        this.pageSizeOptions = [2, 4, 6, 8, 10];
    }
    CommonListarComponent.prototype.ngOnInit = function () {
        this.calcularRangos();
    };
    CommonListarComponent.prototype.paginar = function (event) {
        this.paginaActual = event.pageIndex;
        this.totalPorPagina = event.pageSize;
        this.calcularRangos();
    };
    CommonListarComponent.prototype.calcularRangos = function () {
        var _this = this;
        this.service.listarPaginas(this.paginaActual.toString(), this.totalPorPagina.toString())
            .subscribe(function (paginable) {
            _this.lista = paginable.content; // convierto el atributo content del json a un arreglo de tipo E
            _this.totalRegistros = paginable.totalElements;
            _this.paginator._intl.itemsPerPageLabel = 'Registros por página'; // internacionalización
        });
    };
    CommonListarComponent.prototype.eliminar = function (e) {
        var _this = this;
        sweetalert2_1["default"].fire({
            title: '¡Cuidado!',
            text: "\u00BFSeguro que quiere eliminar a " + e.nombre + "?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!'
        }).then(function (result) {
            if (result.isConfirmed) {
                _this.service.eliminar(e.id).subscribe(function () {
                    _this.calcularRangos();
                    sweetalert2_1["default"].fire('Eliminado: ', "El " + _this.nombreModel + " " + e.nombre + " fue eliminado", 'success');
                });
            }
        });
    };
    __decorate([
        core_1.ViewChild(paginator_1.MatPaginator)
    ], CommonListarComponent.prototype, "paginator");
    return CommonListarComponent;
}());
exports.CommonListarComponent = CommonListarComponent;
