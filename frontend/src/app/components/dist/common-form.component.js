"use strict";
exports.__esModule = true;
exports.CommonFormComponent = void 0;
var sweetalert2_1 = require("sweetalert2");
var CommonFormComponent = /** @class */ (function () {
    function CommonFormComponent(service, // servicio   
    router, // enrutador
    route // obtiene parametros de la ruta
    ) {
        this.service = service;
        this.router = router;
        this.route = route;
    }
    CommonFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            var id = +params.get('id'); // obtengo el parametro 'id' y lo casteo a number con +
            if (id) {
                _this.service.ver(id).subscribe(function (m) { return _this.model = m; });
            }
        });
    };
    CommonFormComponent.prototype.crear = function () {
        var _this = this;
        this.service.crear(this.model).subscribe(function (m) {
            console.log(m);
            sweetalert2_1["default"].fire('Nuevo: ', _this.nombreModel + " " + m.nombre + " creado con \u00E9xito", 'success');
            _this.router.navigate([_this.redirect]);
        }, function (err) {
            if (err.status === 400) {
                _this.error = err.error;
                console.log(_this.error);
            }
        });
    };
    CommonFormComponent.prototype.editar = function () {
        var _this = this;
        this.service.editar(this.model).subscribe(function (m) {
            console.log(m);
            sweetalert2_1["default"].fire('Modificado: ', _this.nombreModel + " " + m.nombre + " actualizado con \u00E9xito", 'success');
            _this.router.navigate([_this.redirect]);
        }, function (err) {
            if (err.status === 400) {
                _this.error = err.error;
                console.log(_this.error);
            }
        });
    };
    return CommonFormComponent;
}());
exports.CommonFormComponent = CommonFormComponent;
