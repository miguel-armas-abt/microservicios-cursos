"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CursoFormComponent = void 0;
var core_1 = require("@angular/core");
var common_form_component_1 = require("../common-form.component");
var curso_1 = require("../../models/curso");
var CursoFormComponent = /** @class */ (function (_super) {
    __extends(CursoFormComponent, _super);
    function CursoFormComponent(service, // servicio   
    router, // enrutador
    route // obtiene parametros de la ruta
    ) {
        var _this = _super.call(this, service, router, route) || this;
        _this.titulo = 'Crear curso';
        _this.model = new curso_1.Curso();
        _this.redirect = '/cursos';
        return _this;
    }
    CursoFormComponent = __decorate([
        core_1.Component({
            selector: 'app-curso-form',
            templateUrl: './curso-form.component.html',
            styleUrls: ['./curso-form.component.css']
        })
    ], CursoFormComponent);
    return CursoFormComponent;
}(common_form_component_1.CommonFormComponent));
exports.CursoFormComponent = CursoFormComponent;
