import { Component, OnInit } from '@angular/core';
import { CommonFormComponent } from '../common-form.component';
import { CursoService } from '../../services/curso.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Curso } from '../../models/curso';

@Component({
  selector: 'app-curso-form',
  templateUrl: './curso-form.component.html',
  styleUrls: ['./curso-form.component.css']
})
export class CursoFormComponent extends CommonFormComponent<Curso, CursoService> implements OnInit {

  constructor(
     service: CursoService, // servicio   
     router: Router, // enrutador
     route: ActivatedRoute // obtiene parametros de la ruta
  ) {
    super(service, router, route);
    this.titulo = 'Crear curso';
    this.model = new Curso();
    this.redirect = '/cursos'; 
   }

}
