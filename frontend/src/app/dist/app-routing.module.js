"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var alumnos_component_1 = require("./components/alumnos/alumnos.component");
var cursos_component_1 = require("./components/cursos/cursos.component");
var examenes_component_1 = require("./components/examenes/examenes.component");
var alumnos_form_component_1 = require("./components/alumnos/alumnos-form.component");
var curso_form_component_1 = require("./components/cursos/curso-form.component");
var examen_form_component_1 = require("./components/examenes/examen-form.component");
// rutas
var routes = [
    { path: '', pathMatch: 'full', redirectTo: 'cursos' },
    { path: 'alumnos', component: alumnos_component_1.AlumnosComponent },
    { path: 'alumnos/form', component: alumnos_form_component_1.AlumnosFormComponent },
    { path: 'alumnos/form/:id', component: alumnos_form_component_1.AlumnosFormComponent },
    { path: 'cursos', component: cursos_component_1.CursosComponent },
    { path: 'cursos/form', component: curso_form_component_1.CursoFormComponent },
    { path: 'cursos/form/:id', component: curso_form_component_1.CursoFormComponent },
    { path: 'examenes', component: examenes_component_1.ExamenesComponent },
    { path: 'examenes/form', component: examen_form_component_1.ExamenFormComponent },
    { path: 'examenes/form/:id', component: examen_form_component_1.ExamenFormComponent },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
