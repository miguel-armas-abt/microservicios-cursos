import { Alumno } from './alumno';
import { Pregunta } from './pregunta';
export class Respuesta {
    id: string; // se maneja en mongodb como string
    texto: string;
    alumno: Alumno;
    pregunta: Pregunta;
}
